/*
 * Copyright 2018 Jose A. Garcia Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <math.h>

#include "logging.h"

typedef struct CartesianPoint
{
    double x; //<! x coordinate
    double y; //<! y coordinate
    double z; //<! z coordinate
} CartesianPoint;

CartesianPoint *point_new(const double x, const double y, const double z)
{
    CartesianPoint *point = (CartesianPoint *)malloc(sizeof(CartesianPoint));
    point->x = x;
    point->y = y;
    point->z = z;

    return point;
}

void point_destroy(CartesianPoint *point)
{
    if (point == NULL)
    {
        logging_log("Nothing to free in here.");
        return;
    }

    free(point);
}

void point_log(const CartesianPoint *point)
{
    logging_log("(%lf, %lf, %lf)", point->x, point->y, point->z);
}

double point_module(const CartesianPoint *point)
{
    return sqrt(pow(point->x, 2.) + pow(point->y, 2.) + pow(point->z, 2.));
}

CartesianPoint *point_sum(const CartesianPoint *point1, const CartesianPoint *point2)
{
    CartesianPoint *added = point_new(point1->x + point2->x, point1->y + point2->y, point1->z + point2->z);

    return added;
}

CartesianPoint *point_multiplyByScalar(const double scalar, const CartesianPoint *point)
{
    CartesianPoint *multiplied = point_new(scalar * point->x, scalar * point->y, scalar * point->z);

    return multiplied;
}

CartesianPoint *point_negate(const CartesianPoint *point)
{
    return point_multiplyByScalar(-1., point);
}
