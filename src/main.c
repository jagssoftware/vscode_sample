/*
 * Copyright 2018 Jose A. Garcia Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "logging.h"
#include "arguments.h"
#include "point.h"
#include "particle.h"

int main(const int argc, const char *argv[])
{
    Arguments *arguments;
    Particle *particle = NULL;
    CartesianPoint *negated = NULL;
    CartesianPoint *multipliedByScalar = NULL;
    CartesianPoint *added = NULL;
    double timeLapse = -1.;
    double temperature = -1.;

    srand((unsigned int)time(NULL));

    arguments = arguments_read(argc, argv);
    if (arguments == NULL)
    {
        logging_log("Not enough parameters, please provide the required ones.");
        return -1;
    }

    temperature = arguments_getTemperature(arguments);
    timeLapse = arguments_getTimeLapse(arguments);

    particle = particle_new(temperature, 0., 0., 0.);

    point_log(particle_getPosition(particle));
    point_log(particle_getVelocity(particle));
    logging_log("|velocity| = [%lf]", pow(point_module(particle_getVelocity(particle)), 2.));
    negated = point_negate(particle_getVelocity(particle));
    point_log(negated);
    multipliedByScalar = point_multiplyByScalar(timeLapse, particle_getVelocity(particle));
    added = point_sum(particle_getPosition(particle), multipliedByScalar);
    point_log(added);

    particle_destroy(particle);
    point_destroy(negated);
    point_destroy(multipliedByScalar);
    point_destroy(added);
    arguments_destroy(arguments);
    return 0;
}
