/*
 * Copyright 2018 Jose A. Garcia Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __POINT_H__
#define __POINT_H__

/**
 * @defgroup coordinate Coordinate
 * @brief Cartesian coordinates component.
 * 
 * 
 * @{
 */
/**
 * @brief Structure with a cartesian point.
 */
typedef struct CartesianPoint CartesianPoint;

/**
 * @brief Create an cartesian point object from the three spatial coordinates.
 * 
 * @param[in] x X coordinate
 * @param[in] y Y coordinate
 * @param[in] z Z coordinate
 * @return Cartesian point object with the provided coordinates
 */
CartesianPoint *point_new(const double x, const double y, const double z);

/**
 * @brief Destroy the cartesian point object.
 * 
 * @warning It is required to make a call to this function, after using the object,
 * in order to free the memory.
 * 
 * @param[in] point Object to be destroyed
 */
void point_destroy(CartesianPoint *point);

/**
 * @brief Writes the content of the object to the log.
 * 
 * @param[in] point Object to be written
 */
void point_log(const CartesianPoint *point);

/**
 * @brief Calculate the module of the cartesian point coordinates.
 * 
 * @param[in] point Object with the coordinates
 * @return Module of the cartesian point coordinates
 */
double point_module(const CartesianPoint *point);

/**
 * @brief Add two points to get a third one.
 * 
 * @warning It creates a new object and returns it. Please be aware of destroying
 * objects not longer in use.
 * 
 * @code
 * CartesianPoint *first = point_new(1., 2., 3.);
 * CartesianPoint *second = point_new(4., 5., 6.);
 * CartesianPoint *sum = point_add(first, second);
 * 
 * // sum has the same coordinates as the object point_new(5., 7., 9.);
 * 
 * point_destroy(first);
 * point_destroy(second);
 * point_destroy(sum);
 * @endcode
 * 
 * @param[in] point1 First point to be added
 * @param[in] point2 Second point to be added
 * @return Sum of the two given points
 */
CartesianPoint *point_sum(const CartesianPoint *point1, const CartesianPoint *point2);

/**
 * @brief Multiplies the coordinate by an scalar value.
 * 
 * @warning It creates a new object and returns it. Please be aware of destroying
 * objects not longer in use.
 * 
 * @code
 * CartesianPoint *point = point_new(1., 2., 3.);
 * CartesianPoint *multiplied = point_multiplyByScalar(point, 7.);
 * 
 * // multiplied has the same coordinates as the object point_new(7., 14., 21.);
 * 
 * point_destroy(point);
 * point_destroy(multiplied);
 * @endcode
 * 
 * @param[in] scalar Scalar to multiply the point with
 * @param[in] point Point to be multiplied
 * @return
 */
CartesianPoint *point_multiplyByScalar(const double scalar, const CartesianPoint *point);

/**
 * @brief Invert the coordinates of the cartesian point, negating all of the coordinates.
 * 
 * @warning It creates a new object and returns it. Please be aware of destroying
 * objects not longer in use.
 * 
 * @code
 * CartesianPoint *point = point_new(1., -2., 3.);
 * CartesianPoint *negated = point_negate(point);
 * 
 * // negated has the same coordinates as the object point_new(-1., 2., -3.);
 * 
 * point_destroy(point);
 * point_destroy(negated);
 * @endcode
 * 
 * @param[in] point Point to be negated
 * @return Negated cartesian coordinate
 */
CartesianPoint *point_negate(const CartesianPoint *point);

/**
 * @}
 */

#endif // __POINT_H__
