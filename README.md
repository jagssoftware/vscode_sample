# README

[Visual Studio Code]: https://code.visualstudio.com/
[CMake]: https://cmake.org
[cmocka]: https://cmocka.org

Sample Cross-Platform C/C++ project developed using [Visual Studio Code].

## Requirements

In order to compile the project, you need to have installed in your computer [CMake]. Along with it, depending on the platform or compiler you want to use, you will need also the corresponding compiler and/or build engine tool (i.e., ```msbuild```, ```make```, ...)

### MSBuild

If you have installed any version of ```Visual Studio``` you have already in your windows system the ```msbuild``` tool.

However, if you need to use in any case ```msbuild``` but you don't want to pay the license for ```Visual Studio```, or you are using any other platform than ```Windows```, then you can download/clone the project for your system from [here](https://github.com/Microsoft/msbuild).

### Make

You can install in your system the tools ```g++```, ```make```, ...

## Compilation

Once [CMake] and the building tools are installed in your system, then you can go to ```build``` directory and from the command prompt, run the command ```cmake```, with the corresponding argument depending on your system.

The project is using [cmocka] as unit testing framework, therefore references to the library and include files must be provided.

### Unix/Linux

        build/ $> cmake -G "Unix Makefiles" ..
        build/ $> make

To clean the compiled objects:

        build/ $> make clean

### Windows

        C:\build> cmake -G "Visual Studio 11 2012 Win64" -DCMOCKA_HOME=<cmocka installation directory> ..
        C:\build> msbuild dynamics.sln

**Note:** In the example the solution has been generated for _Visual Studio 2012_ and platform _64_ bits. However you can choose the combination you're interested in.

To clean the compiled objects:

        C:\build> msbuild dynamics.sln /t:clean

## Testing

Along with the code, testing code is delivered and compiled when compiling the code.
To run the tests, there are two options:

1. using ```ctest``` command, it runs all tests at once. This command belongs to the same toolset as ```cmake```.

        # Linux
        build/ $> ctest

        REM Windows
        C:\build> ctest

1. Running the tests individually. After the compilation, the tests are kept in ```build\test\Debug``` (Windows) or ```build/test``` (Linux)

### CMocka configuration

Because it has been used [cmocka] as unit testing framework, there are futher configuration to create the reports as jUnit style in xml format. The  configuration is achieved using the following environment variables:

* ```CMOCKA_MESSAGE_OUTPUT```, with the following values:

  * ```STDOUT``` for the default standard output printer
  * ```SUBUNIT``` for subunit output
  * ```TAP``` for Test Anythng Protocol (TAP) output
  * ```XML``` for xUnit XML format

* ```CMOCKA_XML_FILE```, to set in which file to write the result of the tests. You can give the value ```cm_%g.xml``` in order to create one xml file per group of tests. ```%g``` takes the group name of the test under execution.

For example, before executing the tests, set the varibles:

        build/ $> export CMOCKA_MESSAGE_OUTPUT=xml
        build/ $> export CMOCKA_XML_FILE=cm_%g.xml

in Linux, or

        C:\build $> set CMOCKA_MESSAGE_OUTPUT=xml
        C:\build $> set CMOCKA_XML_FILE=cm_%g.xml

in Windows.

The xml reports will be created beside the test executables, as it has been set in
the environment variable ```CMOCKA_XML_FILE```.

Check the official documentation from [cmocka] for further and detailed documentation.

### Code Coverage (Only Linux)

One of the features of GCC compilers is that it can create code coverage reports.
To create the reports, the following commands and tools must be installed in the system:

* [gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) is a tool to be used in conjuction with GCC to test code coverage
* [lcov](http://ltp.sourceforge.net/coverage/lcov.php) is a graphical front-end for GCC's coverage testing tool ```gcov```.

If the coverage functionality has been set when compiling and linking the programs, after the compilation you can find on your workplace several ```*.gcno``` files, one per source code file.

After running the tests (```ctest```, for example), some ```*.gcda``` files are created. This files contain the coverage information of the source code.

After having executed the tests, execute the command to create the info file before creating the report:

        build/ $> lcov -c -d <parent directory with the *.gcda> -o <PROJ_ROOT>/coverage.info

To create a html report of the coverage, run the command:

        build/ $> genhtml <PROJ_ROOT>/coverage.info -o <PROJ_ROOT>/reports/coverage
