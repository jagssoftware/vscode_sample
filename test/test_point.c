/*
 * Copyright 2018 Jose A. Garcia Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdio.h>

#include "util_test.h"
#include "point.c"


// Object under test
static CartesianPoint *underTest;

// Buffer where the log is written
static char buffer[256];

// Mock function
void logging_log(const char *string, ...)
{
    va_list args;
    va_start(args, string);
    vsprintf(buffer, string, args);

    check_expected_ptr(buffer);

    va_end(args);
}

static void test_point_destroy_null_object(void **state)
{
    (void)state;

    expect_string(logging_log, buffer, "Nothing to free in here.");

    point_destroy(NULL);
}

static void test_point_log(void **state)
{
    (void)state;
    expect_string(logging_log, buffer, "(1.000000, 2.000000, 3.000000)");

    point_log(underTest);
}

static void test_point_module(void **state)
{
    (void)state;

    assert_double_equal(3.741657, point_module(underTest), 0.00001);
}

static void test_point_sum(void **state)
{
    CartesianPoint *addend1 = point_new(1., 2., 3.);
    CartesianPoint *addend2 = point_new(.5, .6, .7);
    CartesianPoint *sum = point_sum(underTest, addend2);
    (void)state;

    assert_double_equal(1.5, sum->x, 0.00001);
    assert_double_equal(2.6, sum->y, 0.00001);
    assert_double_equal(3.7, sum->z, 0.00001);

    point_destroy(addend1);
    point_destroy(addend2);
    point_destroy(sum);
}

static void test_point_multiplyByScalar(void **state)
{
    CartesianPoint *multiplied = point_multiplyByScalar(.5, underTest);
    (void)state;

    assert_double_equal(.5, multiplied->x, 0.00001);
    assert_double_equal(1., multiplied->y, 0.00001);
    assert_double_equal(1.5, multiplied->z, 0.00001);

    point_destroy(multiplied);
}

static void test_point_negate(void **state)
{
    CartesianPoint *negated = point_negate(underTest);
    (void)state;

    assert_double_equal(-1., negated->x, 0.0001);
    assert_double_equal(-2., negated->y, 0.0001);
    assert_double_equal(-3., negated->z, 0.0001);

    point_destroy(negated);
}

static int group_setup(void **state)
{
    (void)state;
    underTest = point_new(1., 2., 3.);
    buffer[0] = '\0';
    return 0;
}

static int group_tearDown(void **state)
{
    (void)state;
    point_destroy(underTest);
    return 0;
}

int main(void)
{
    const struct CMUnitTest tests_point[] = {
        cmocka_unit_test(test_point_destroy_null_object),
        cmocka_unit_test(test_point_log),
        cmocka_unit_test(test_point_module),
        cmocka_unit_test(test_point_sum),
        cmocka_unit_test(test_point_multiplyByScalar),
        cmocka_unit_test(test_point_negate)};

    return cmocka_run_group_tests(tests_point, group_setup, group_tearDown);
}
