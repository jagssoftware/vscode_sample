/*
 * Copyright 2018 Jose A. Garcia Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdio.h>

#include "util_test.h"
#include "particle.c"

static char buffer[256];         //<! Buffer where the log is written
static CartesianPoint *position; //<! Position of the particle under test
static CartesianPoint *velocity; //<! Velocity of the particle under test
static Particle *underTest;      //<! Particle under test

// Mock function
void logging_log(const char *string, ...)
{
    va_list args;
    va_start(args, string);
    vsprintf(buffer, string, args);

    check_expected_ptr(buffer);

    va_end(args);
}

/**
 * @brief Mock function
 * 
 * @return
 */
double randomUniform()
{
    return (double)mock();
}

/**
 * @brief Mock function
 * 
 * @param[in] x
 * @param[in] y
 * @param[in] z
 * @return
 */
CartesianPoint *point_new(const double x, const double y, const double z)
{
    check_expected(x);
    check_expected(y);
    check_expected(z);

    return (CartesianPoint *)mock();
}

/**
 * @brief Mock function.
 * 
 * @param[in] point
 */
void point_destroy(CartesianPoint *point)
{
    check_expected(point);
}

static void test_particle_destroy_null_object(void **state)
{
    (void)state;
    expect_string(logging_log, buffer, "Nothing to free in here.");

    particle_destroy(NULL);
}

static void test_particle_getPosition(void **state)
{
    CartesianPoint *actual = NULL;
    (void)state;

    actual = particle_getPosition(underTest);

    assert_ptr_not_equal(NULL, actual);
    assert_ptr_equal(position, actual);
}

static void test_particle_getVelocity(void **state)
{
    CartesianPoint *actual = NULL;
    (void)state;

    actual = particle_getVelocity(underTest);

    assert_ptr_not_equal(NULL, actual);
    assert_ptr_equal(velocity, actual);
}

static void test_particle_getTemperature(void **state)
{
    (void)state;
    assert_double_equal(.4, particle_getTemperature(underTest), 0.0001);
}

static int group_setup(void **state)
{
    (void)state;
    expect_value(point_new, x, 1.2);
    expect_value(point_new, y, 2.3);
    expect_value(point_new, z, 3.4);
    expect_value(point_new, x, -0.04320);
    expect_value(point_new, y, 0.07518);
    expect_value(point_new, z, -0.04982);

    position = (CartesianPoint *)malloc(3 * sizeof(double));
    velocity = (CartesianPoint *)malloc(3 * sizeof(double));

    will_return(point_new, position);
    will_return(point_new, velocity);
    will_return(randomUniform, .666);
    will_return(randomUniform, .333);

    underTest = particle_new(.4, 1.2, 2.3, 3.4);
    return 0;
}

static int group_tearDown(void **state)
{
    (void)state;

    expect_value(point_destroy, point, position);
    expect_value(point_destroy, point, velocity);

    free(position);
    free(velocity);
    particle_destroy(underTest);
    return 0;
}

int main(void)
{
    const struct CMUnitTest tests_particle[] = {
        cmocka_unit_test(test_particle_getPosition),
        cmocka_unit_test(test_particle_getVelocity),
        cmocka_unit_test(test_particle_getTemperature),
        cmocka_unit_test(test_particle_destroy_null_object)};

    return cmocka_run_group_tests(tests_particle, group_setup, group_tearDown);
}
