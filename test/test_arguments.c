/*
 * Copyright 2018 Jose A. Garcia Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdio.h>

#include "util_test.h"
#include "arguments.c"

// Object under test
static Arguments *underTest;

// Buffer where the log is written
static char buffer[256];

// Mock function
void logging_log(const char *string, ...)
{
    va_list args;
    va_start(args, string);
    vsprintf(buffer, string, args);

    check_expected_ptr(buffer);

    va_end(args);
}

static void test_arguments_read_lessThat3Arguments(void **state)
{
    const char *argv[] = {"program", "1.75"};
    Arguments *arguments = arguments_read(2, argv);
    (void)state;

    assert_null(arguments);
}

static void test_arguments_destroy_null_object(void **state)
{
    (void)state;

    expect_string(logging_log, buffer, "Nothing to free here");

    arguments_destroy(NULL);
}

static void test_arguments_getTemperature(void **state)
{
    (void)state;

    assert_double_equal(1.75, arguments_getTemperature(underTest), 0.0001);
}

static void test_arguments_getTimeLapse(void **state)
{
    (void)state;

    assert_double_equal(8.76, arguments_getTimeLapse(underTest), 0.0001);
}

static int setUp(void **state)
{
    const char *argv[] = {"program", "1.75", "8.76"};

    (void)state;
    underTest = arguments_read(3, argv);

    return 0;
}

static int tearDown(void **state)
{
    (void)state;
    arguments_destroy(underTest);

    return 0;
}

int main(void)
{
    const struct CMUnitTest tests_arguments[] = {
        cmocka_unit_test(test_arguments_read_lessThat3Arguments),
        cmocka_unit_test(test_arguments_destroy_null_object),
        cmocka_unit_test(test_arguments_getTemperature),
        cmocka_unit_test(test_arguments_getTimeLapse)};

    return cmocka_run_group_tests(tests_arguments, setUp, tearDown);
}
